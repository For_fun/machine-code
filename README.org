#+TITLE: Machine code 🤖
#+CREATOR: PyerineVanboline
#+LANGUAGE: en

[[https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/e483edbf-4b49-42e8-a684-cdf17c4f227f/ddbueir-69ae477b-4b68-4914-9d24-1d4a9258af04.png/v1/fill/w_538,h_350,strp/amstrad_cpc_png_by_framerater_ddbueir-350t.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NjU3IiwicGF0aCI6IlwvZlwvZTQ4M2VkYmYtNGI0OS00MmU4LWE2ODQtY2RmMTdjNGYyMjdmXC9kZGJ1ZWlyLTY5YWU0NzdiLTRiNjgtNDkxNC05ZDI0LTFkNGE5MjU4YWYwNC5wbmciLCJ3aWR0aCI6Ijw9MTAwOSJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.3rPUwadY-667EbgO1Vc3slqD1phbltnQT5kWz-v6H78]]

* About this project
Machine code programs that i made for fun and for learn more about low level programming ( Only for [[https://en.wikipedia.org/wiki/Amstrad_CPC][Amstrad cpc]]).
* About me
In this repository, i tell my experience writting machine code and assembly code.
* Memory video
Here are my notes, about the video memory of the Amstrad CPC.
** Colors binary
- 00 = Blue
- 01 = Yellow
- 10 = Light blue
- 11 = Red
** Default position of colors
|---+---+---+---+---+---+---+---|
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---+---+---+---+---+---+---+---|
| 0 | 1 | 0 | 1 | 0 | 0 | 1 | 1 |
|---+---+---+---+---+---+---+---|
Binary   ----->    Hexdecimal
0 1 0 1 0 0 1 1 ---> 53

** Colors Hexadecimal
*** Red
|---+---+---+---+---+---+---+---|
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---+---+---+---+---+---+---+---|
| 1 | 1 | 1 | 1 | 1 | 1 | 1 | 1 |
|---+---+---+---+---+---+---+---|
1 1 1 1 | 1 1 1 1 = ff
*** Light blue
|---+---+---+---+---+---+---+---|
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---+---+---+---+---+---+---+---|
| 0 | 0 | 0 | 0 | 1 | 1 | 1 | 1 |
|---+---+---+---+---+---+---+---|
0 0 0 0  1 1 1 1 = 0f

*** Yellow
|---+---+---+---+---+---+---+---|
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---+---+---+---+---+---+---+---|
| 1 | 1 | 1 | 1 | 0 | 0 | 0 | 0 |
|---+---+---+---+---+---+---+---|
1 1 1 1  0 0 0 0 = 70

*** Blue 
|---+---+---+---+---+---+---+---|
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---+---+---+---+---+---+---+---|
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
|---+---+---+---+---+---+---+---|
0 0 0 0  0 0 0 0 = 00
** Zone of pruebes
|---+---+---+---+---+---+---+---|
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---+---+---+---+---+---+---+---|
| 0 | 0 | 0 | 0 | 0 | 1 | 1 | 1 |
|---+---+---+---+---+---+---+---|
0 0 0 0 | 1 0 0 1 = 07
** First Program
#+begin_src 
3e 53 32 00 c0 18 fe
#+end_src
** Challenges
*** First challenge
Now i want to draw this:
| Yellow | Red | Red | Yellow |
**** Table
|---+---+---+---+---+---+---+---|
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---+---+---+---+---+---+---+---|
| 1 | 1 | 1 | 1 | 0 | 1 | 1 | 0 |
|---+---+---+---+---+---+---+---|
1 1 1 1 | 0 1 1 0 =  f6
Yes!!, i did it
*** Second  Challenge
Now i want to draw this urig the instrucion "21":
| Red | Yellow | light blue | Yellow |  |light blue | Yellow | light blue | Red |
**** Table
|---+---+---+---+---+---+---+---|
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---+---+---+---+---+---+---+---|
| 1 | 1 | 0 | 1 | 1 | 0 | 1 | 0 |
|---+---+---+---+---+---+---+---|
|---+---+---+---+---+---+---+---|
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---+---+---+---+---+---+---+---|
| 0 | 1 | 0 | 1 | 1 | 0 | 1 | 1 |
|---+---+---+---+---+---+---+---|
1 1 0 1 | 1 0 1 0 =  da
0 1 0 1 | 1 0 1 1 =  5b
Yes!!, i did it

* Programs
** Sprites
*** Program 1
#+begin_src 
3e 01 | 32 32 c2 | 18 fe
3e 10 | 32 34 c2 | 18 fe
3e 11 | 32 36 c2 | 18 fe
#+end_src
The result is:
[[result1.png]]
*** Program 2
#+begin_src 
3e 11 32 fc c0 18 fe
3e 10 32 fc c8 18 fe
3e 01 32 fc d0 18 fe
#+end_src
The result is: 
[[result2.png]]
*** Program 3
#+begin_src
21 99 22 | 22 fc c0 |18 fe
21 90 20 | 22 fc c8 |18 fe
21 09 02 | 22 fc d0 |18 fe
#+end_src
The result is: 
[[result3.png]]

*** Program 4
 Basically, this draws colored lines in a row. There are 4 pixels per row, that's 8 rows.
#+begin_src 
3e 0f 32 00 c0 18 fe  #light blue
3e f0 32 00 c8 18 fe  #Yellow
3e ff 32 00 d0 18 fe  #
3e 00 32 00 d8 18 fe
3e 0f 32 00 e0 18 fe
3e f0 32 00 e8 18 fe
3e ff 32 00 f0 18 fe
3e 00 32 00 f8 18 fe
#+end_src
*** Program 5
This draws 8 lines under the word ready, they are of 4 possible colors.
**** *First lines*
***** Red Line
#+begin_src
21 ff ff | 22 d0 c2 | 22 d2 c2 | 18 fe
21 ff ff | 22 d4 c2 | 22 d6 c2 | 18 fe
21 ff ff | 22 d8 c2 | 18 fe
#+end_src
***** Blue Line
#+begin_src
21 00 00 | 22 d0 ca | 22 d2 ca | 18 fe
21 00 00 | 22 d4 ca | 22 d6 ca | 18 fe
21 00 00 | 22 d8 ca | 18 fe
#+end_src
***** yellow Line
#+begin_src
21 f0 f0 | 22 d0 d2 | 22 d2 d2 | 18 fe
21 f0 f0 | 22 d4 d2 | 22 d6 d2 | 18 fe
21 f0 f0 | 22 d8 d2 | 18 fe
#+end_src
***** Light blue Line
#+begin_src
21 0f 0f | 22 d0 da | 22 d2 da | 18 fe
21 0f 0f | 22 d4 da | 22 d6 da | 18 fe
21 0f 0f | 22 d8 da | 18 fe
#+end_src
**** *second Lines lines*
***** Red Line
#+begin_src
21 ff ff | 22 d0 e2 | 22 d2 e2 | 18 fe
21 ff ff | 22 d4 e2 | 22 d6 e2 | 18 fe
21 ff ff | 22 d8 e2 | 18 fe
#+end_src
***** Blue Line
#+begin_src
21 00 00 | 22 d0 ea | 22 d2 ea | 18 fe
21 00 00 | 22 d4 ea | 22 d6 ea | 18 fe
21 00 00 | 22 d8 ea | 18 fe
#+end_src
***** yellow Line
#+begin_src
21 f0 f0 | 22 d0 f2 | 22 d2 f2 | 18 fe
21 f0 f0 | 22 d4 f2 | 22 d6 f2 | 18 fe
21 f0 f0 | 22 d8 f2 | 18 fe
#+end_src
***** Light blue Line
#+begin_src
21 0f 0f | 22 d0 fa | 22 d2 fa | 18 fe
21 0f 0f | 22 d4 fa | 22 d6 fa | 18 fe
21 0f 0f | 22 d8 fa | 18 fe
#+end_src
*** Program 6
#+begin_src 
RED
21 ff ff | 22 9e f9 | 22 9e f1 | c3 10 40
21 ff ff | 22 a0 f9 | 22 a0 f1 | c3 20 40
21 ff ff | 22 a2 f9 | 22 a2 f1 | c3 30 40
-----------------------------------------
21 ff ff | 22 3e c2 | 22 3e ca | c3 40 40
21 ff ff | 22 40 c2 | 22 40 ca | c3 50 40
21 ff ff | 22 42 c2 | 22 42 ca | c3 60 40

Light Blue 
3e 03 | 32 9d f9 | 32 9d f1 | 32 ed c1 | c3 70 40 
3e 03 | 32 ed c9 | 32 ed d1 | 32 ed d9 | c3 80 40
3e 03 | 32 ed e1 | 32 ed e9 | 32 ed f1 | c3 90 40
3e 03 | 32 ed f9 | 32 3d c2 | 32 3d ca | c3 a0 40
#+end_src
*** Program 7 (semy sprite)
#+begin_src 
21 f0 f0 22 e6 c3 22 e7 c3 c3 10 40  
21 f6 f6 22 e6 cb 22 e7 cb c3 20 40
21 f0 f0 22 e6 d3 22 e7 d3 c3 30 40
21 f0 f0 22 e6 db 22 e7 db c3 40 40
21 f0 f0 22 e6 e3 22 e7 e3 c3 50 40
21 f6 f6 22 e6 eb 22 e7 eb c3 60 40
21 f6 f6 22 e6 f3 22 e7 f3 c3 18 f5
#+end_src
The result is: 
[[result4.png]]

*** Program 8 (The sprite)
#+begin_src 
4000- 3e 0f 32 e6 c3 32 e7 c3 3e 01 32 e5 cb 3e ff 32 
4010- e6 cb 32 e7 cb 3e 08 32 e8 cb 3e 13 32 e5 d3 3e
4020- ff 32 e6 d3 32 e7 d3 3e 8c 32 e8 d3 3e 06 32 e5
4030- db 3e ff 32 e6 db 32 e7 db 3e 8c 32 e8 db 3e 08
4040- 32 e5 e3 3e 77 32 e6 e3 3e ff 32 e7 e3 3e 0e 32  
4050- e8 e3 3e 17 32 e5 eb 3e ef 32 e6 eb 3e 0f 32 e7 
4060- eb 3e 0e 32 e8 eb 3e 0f 32 e7 eb 3e 0e 32 e8 eb
4070- 3e 02 32 e5 f3 3e 08 32 e6 f3 3e 0f 32 e7 f3 3e
4080- 0c 32 e8 f3 3e 02 32 e5 fb 3e 08 32 e6 fb 3e 08
4090- 32 e7 fb 3e 0c 32 e8 fb 3e 02 32 35 c4 3e 08 32  
40a0- 38 c4 3e 01 32 35 cc 3e 03 32 37 cc 3e 84 32 38 
40b0- cc 3e 0f 32 36 d4 3e 1e 32 37 d4 3e 84 32 38 d4  
40c0- 3e 06 32 36 dc 3e 01 32 37 dc 3e 08 32 38 dc 3e  
40d0- 06 32 36 e4 3e 01 32 37 e4 3e 84 32 38 e4 3e 09 
40e0- 32 36 ec 3e 0f 32 37 ec 3e 08 32 38 ec 3e 08 32
40f0- 36 f4 3e 01 32 37 f4 3e 07 32 36 fc 3e 0e 32 37
4100- fc c3 00 40
#+end_src
The result is: 
[[Final_Sprite.png]]

** Animations
*** Part 1 
**** program 1
This Program is a animation, which is a simple red pixel moving.
#+begin_src 
3e 88 32 00 c0 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 3e 44 32 00 c0 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 3e 22 32 00 c0 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 3e 11 32 00 c0 c3 00 40
#+end_src
**** Program 2
This program is a animation,which is like a progress bar
#+begin_src
3e ff 32 00 c0 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 3e 08 32 00 c0 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 3e 0c 32 00 c0 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 3e 0e 32 00 c0 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 3e 0f 32 00 c0 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 3e 80 32 00 c0 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 3e c0 32 00 c0 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 3e e0 32 00 c0 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 3e f0 32 00 c0 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 3e 70 32 00 c0 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 3e 70 32 00 c0 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 3e 30 32 00 c0 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 3e 10 32 00 c0
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 3e 00 32 00
c0 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 c3 00 40 
#+end_src
**** Program 3
This program is an sprite and animation
#+begin_src
21 70 e0 22 00 c0 21 52 a4 22 00 c8 21 f0 f0
22 00 d0 21 f0 f0 22 00 d8 21 71 e0 22 00 e0
21 70 e0 22 00 e8 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 3e e8 32 01 e0 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 21 73 ec 22
00 e0 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 21 71 e8 22 00 e0 21 f2 f4 22 00 d8 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 c3
00 40  
#+end_src
*** Part 2 
**** Program 4
this is a line which change of his position
#+begin_src 
21 00 c0 36 ff 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 36 00 21 01 c0 36 ff 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 36 00 21 02 c0 36 ff 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 36 00 21 03 c0 36 ff 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 36 00 c3 00 40
#+end_src
**** Program 5
#+begin_src
21 00 c0 36 fd 26 c8 36 fb 21 06 c0 36 fc 26 c8 36 f8 
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 36 f0 26 c0 36 f0
21 05 c0 36 fc 26 c8 36 f8 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 36 f0 26 c0 36 f0 21 04 c0 36 fc 26 c8 36 f8
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 36 f0 26 c0 36
f0 21 03 c0 36 fc 26 c8 36 f8 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 36 f0 26
c0 36 f0 21 02 c0 36 fc 26 c8 36 f8 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 36 f0 26 c0 36 f0 21 01 c0 36 fc 26 c8
36 f8 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 36 00 26 c0
36 00 21 02 c0 36 fc 26 c8 36 f8 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 36 00 26 c0 36 00 21 03 c0 36 fc
26 c8 36 f8 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 36 00 26 c0 36 00 21 04 c0 36 fc 26 c8 36 f8 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 36 00 26 c0 36 00 21 05 c0 36 fc
26 c8 36 f8 76 76 76 76 76 76 76 76 76 76 76 76 76 76
76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 76 36 00
26 c0 36 00 21 06 c0 36 fc 26 c8 36 f8 c3 00 40   
#+end_src

21 00 c0 36 fc 26 c8 36 f8

|------+------+------+------+------+------+------|
| c000 | c001 | c002 | c003 | c004 | c005 | c006 |
|------+------+------+------+------+------+------|
|    4 |   24 |   20 |   16 |   12 |    8 |    4 |
|------+------+------+------+------+------+------|

fc = 1111 1100

a = 10
b= 11
c= 12
**** Program 6 
#+begin_src 
21 02 c0 36 11 2e 03 36 88 21 02 c8 36 32 2e 03 36 c4 21 02 d0 36 32 2e 03 36 c4  
#+end_src
1100 0100 = c4

Less fast /\
 O
||
 O
||
 O
||
 O
More fast \/
